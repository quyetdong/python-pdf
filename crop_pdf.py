import fitz                     # Load thư viện PyMuPDF module
from PIL import Image
import sys, json

# Improve 1: Calculate the distance between two lines (font height) to get the appropriate offset 
# Improve 2: Search start word for each question in the Exam by Regex (or some format)

# Improve 3: allow user input to set format/Regex/Offset

# Improve resolution
# List of accepted start words

# Read text from image in pdf


def getMinCoorX(blockList):
    mapBlockList = [x[0] for t, x in enumerate(blockList)]
    return min(mapBlockList)

def getMaxCoorX(blockList):
    mapBlockList = [x[2] for t, x in enumerate(blockList)]
    return max(mapBlockList)

def getMinCoorY(blockList, minY = 0):
    mapBlockList = [x[1] for t, x in enumerate(blockList) if x[1] > minY]
    return min(mapBlockList)

def getMaxCoorY(blockList):
    mapBlockList = [x[3] for t, x in enumerate(blockList)]
    return max(mapBlockList)

def getCoorX(blocklist, notStartWords):
    blocklist = [x for t, x in enumerate(blocklist) if (not x[4].strip().startswith(tuple(notStartWords)) and x[4].strip())]
    
    textCorX1 = getMinCoorX(blocklist)
    textCorX2 = getMaxCoorX(blocklist)

    return [textCorX1, textCorX2]

def getDocCoorX(doc, notStartWords):
    coorX1List = []
    coorX2List = []
    for page in doc:
        blocks = page.get_text_blocks()
        blocklist = [x for t, x in enumerate(blocks) if (not x[4].strip().startswith(tuple(notStartWords)) and x[4].strip())]
        
        coorX1List.append(getMinCoorX(blocklist))
        coorX2List.append(getMaxCoorX(blocklist))

    return [min(coorX1List), max(coorX2List)]


def SortBlocks(blocks):
    '''
    Sort the blocks of a TextPage in ascending vertical pixel order,
    then in ascending horizontal pixel order.
    This should sequence the text in a more readable form, at least by
    convention of the Western hemisphere: from top-left to bottom-right.
    If you need something else, change the sortkey variable accordingly ...
    '''

    sblocks = []
    for b in blocks:
        x0 = str(int(b["bbox"][0]+0.99999)).rjust(4,"0") # x coord in pixels
        y0 = str(int(b["bbox"][1]+0.99999)).rjust(4,"0") # y coord in pixels
        
        sortkey = y0 + x0                                # = "yx"
        sblocks.append([sortkey, b])

    sblocks.sort(key=lambda x: (x[0], x[1]['bbox'][1], x[1]['bbox'][0]), reverse=False)
  
    return [b[1] for b in sblocks] # return sorted list of blocks

def SortLines(lines):
    ''' Sort the lines of a block in ascending vertical direction. See comment
    in SortBlocks function.
    '''

    slines = []
    for l in lines:
        y0 = l["bbox"][1]
        slines.append([y0, l])

    slines.sort(key=lambda x: (x[0], x[1]['bbox'][0]), reverse=False)

    return [l[1] for l in slines]

def SortSpans(spans):
    ''' Sort the spans of a line in ascending horizontal direction. See comment
    in SortBlocks function.
    '''
    sspans = []
    for s in spans:
        x0 = s["bbox"][0]
        sspans.append([x0, s])
    sspans.sort()
    return [s[1] for s in sspans]

def FilterTextBlocks(textBlocks, startWords):
    newBlocks = []

    for b in textBlocks:
        if "lines" in b:
            for l in b["lines"]:
                if "spans" in l:
                    for s in l["spans"]:
                        if "text" in s and s["text"].strip().startswith(tuple(startWords)):
                            newBlocks.append(b)

    return newBlocks


def cropPdf3(
    minLineHeight,
    offsets,
    pdfName, 
    startWords, 
    imgPrefix,
    directory,
    notStartWords):

    doc = fitz.open(pdfName)
    pages = doc.page_count

    zoom = 2    # zoom factor
    mat = fitz.Matrix(zoom, zoom)

    images = []
    widths = []
    heights = []
    image_numb = 0
    textCorX1, textCorX2 = getDocCoorX(doc, notStartWords)

    for p_numb in range(pages):
        fitz_page = doc[p_numb]

        # get min and max coordinate for x-axis
        blocklist = fitz_page.get_text_blocks()
        blocklist = [x for t, x in enumerate(blocklist) if (not x[4].strip().startswith(tuple(notStartWords)) and x[4].strip())]

        textCorY1 = getMinCoorY(blocklist)
        textCorY2 = None

        pg = doc.load_page(p_numb)                         # load page number i
        text = pg.get_text('json')           # get its text in JSON format

        pgdict = json.loads(text)                   # create a dict out of it

        # if (p_numb == 0):
        #     print(json.dumps(pgdict["blocks"], sort_keys=True, indent=4))
        textBlocks = FilterTextBlocks(pgdict["blocks"], startWords)
        blocks = SortBlocks(textBlocks)        # now re-arrange ... blocks

        textCorY = []
        for b in blocks:
            lines = []

            if "lines" in b:
                lines = SortLines(b["lines"])            # ... lines
            else: continue


            for l in lines:
                spans = SortSpans(l["spans"])        # ... spans
                s = spans[0]
                spanCoors = [x["bbox"] for t, x in enumerate(spans)]

                # if p_numb == 1:
                #     print(spans)
                #     print("\n")

                if s["text"].strip().startswith(tuple(startWords)):
                    textCorY.append(getMinCoorY(spanCoors))

        for y_numb in range(len(textCorY)):
            if p_numb > 0 and y_numb == 0 and textCorY[0] - textCorY1 >= minLineHeight :
                textCorY2 = textCorY[0]
                if str(image_numb) in offsets:
                    textCorY2 = textCorY2 - offsets.get(str(image_numb))

                # crop the area
                fitz_page.setCropBox(fitz.Rect(textCorX1, textCorY1, textCorX2, textCorY2))
                # get pix map
                pix=fitz_page.getPixmap(matrix = mat)

                tempImg = Image.frombytes("RGB", [pix.width, pix.height], pix.samples)
                images.append(tempImg)
                widths.append(pix.width)
                heights.append(pix.height)

                maxWidth = max(widths)
                totalHeight = sum(heights)
                new_im = Image.new('RGB', (maxWidth, totalHeight))
                x_offset = 0
                for im in images:
                    new_im.paste(im, (0,x_offset))
                    x_offset += im.size[1]

                base_name_highlight= imgPrefix + str(image_numb - 1) + ".png"
                new_im.save(f'./{directory}/' + base_name_highlight)

            textCorY1 = textCorY[y_numb]

            if (y_numb < len(textCorY) - 1):
                textCorY2 = textCorY[y_numb + 1]
            else:
                textCorY2 = getMaxCoorY(blocklist)
            
            if str(image_numb + 1) in offsets and y_numb != len(textCorY) - 1:
                textCorY2 = textCorY2 - offsets.get(str(image_numb + 1))
            if str(image_numb) in offsets:
                textCorY1 = textCorY1 - offsets.get(str(image_numb))

            # here i set the cropping area around the annotated text
            fitz_page.setCropBox(fitz.Rect(textCorX1, textCorY1, textCorX2, textCorY2))
            # get pix map
            pix=fitz_page.getPixmap(matrix = mat)

            # saving the cropped area as png file
            base_name_highlight = imgPrefix + str(image_numb) + ".png"
            pix.writeImage(f'./{directory}/' + base_name_highlight)

            if y_numb == len(textCorY) - 1:
                preTempImg = Image.frombytes("RGB", [pix.width, pix.height], pix.samples)
                images = [preTempImg]
                widths = [pix.width]
                heights = [pix.height]
        
            image_numb += 1

                   


# fileName = "pdf_docs/Mau-PDF/kiem-tra-giai-tich-12-chuong-1-nam-2019-2020-truong-nguyen-trung-thien-ha-tinh.pdf"
fileName = "pdf_docs/1_De-Chinhthuc-Vatli-K18-M224-pdf.pdf"


fileName1 = "pdf_docs/1_De-Chinhthuc-Vatli-K18-M224-pdf.pdf"
fileName2 = "pdf_docs/1-De-thi-tham-khao-TN-THPT-2020-Ngu-van.pdf"
fileName3 = "pdf_docs/2 Đề mẫu chuẩn (convert PDF).pdf"
fileName4 = "pdf_docs/3 [Tailieuvui.com]_de-thi-thpt-quoc-gia-2020-mon-toan-cua-bo-gddt-de-104.pdf"
fileName5 = "pdf_docs/4 bo-de-thi-thu-thpt-quoc-gia-nam-2017-mon-toan-so-1.pdf"
fileName6 = "pdf_docs/5-De-thi-tham-khao-TN-THPT-2020-Sinh-hoc.pdf"
fileName7 = "pdf_docs/Mau-PDF/20-sample-ket-01.pdf"
fileName8 = "pdf_docs/Mau-PDF/dap-an-de-tham-khao-2021-mon-tieng-anh.pdf"
fileName9 = "pdf_docs/Mau-PDF/de-hsg-toan-10-nam-2020-2021-cum-thpt-huyen-yen-dung-bac-giang.pdf"


directory = "images_output/Mau-PDF"


directory1 = "images_output/1_De-Chinhthuc-Vatli-K18-M224-pdf"
directory2 = "images_output/1-De-thi-tham-khao-TN-THPT-2020-Ngu-van"
directory3 = "images_output/2 Đề mẫu chuẩn (convert PDF)"
directory4 = "images_output/3 [Tailieuvui.com]_de-thi-thpt-quoc-gia-2020-mon-toan-cua-bo-gddt-de-104"
directory5 = "images_output/4 bo-de-thi-thu-thpt-quoc-gia-nam-2017-mon-toan-so-1"
directory6 = "images_output/5-De-thi-tham-khao-TN-THPT-2020-Sinh-hoc"
directory7 = "images_output/20-sample-ket-01"
directory8 = "images_output/dap-an-de-tham-khao-2021-mon-tieng-anh"
directory9 = "images_output/de-hsg-toan-10-nam-2020-2021-cum-thpt-huyen-yen-dung-bac-giang"


# cropPdf3(
#     minLineHeight = 10,
#     offsets = { "2": 1, "3": 6, "4": 10, "6": 3, "7": 2, "8": 5, "9": 5 },
#     startWords = ["Cau", "Câu", "Question", "Questions", "Bài", "WRITING TASK", "PART", "ĐỌC HIỂU"],
#     notStartWords = ["Trang", "VnDoc"], 
#     pdfName = fileName, 
#     imgPrefix = "output_",
#     directory = directory)

cropPdf3(
    minLineHeight = 10,
    offsets = { "1": 2, "2": 10, "3": 2, "4": 10, "5": 3, 
    "6": 3, "7": 2, "8": 1, "9": 5, "16": 5, "26": 6, "27": 10 },
    startWords = ["Cau", "Câu", "Question", "Questions", "Bài", "WRITING TASK", "PART", "ĐỌC HIỂU"],
    notStartWords = ["Trang", "VnDoc"], 
    pdfName = fileName1, 
    imgPrefix = "output_",
    directory = directory1)


# cropImage(fileName3)
