import fitz                     # Load thư viện PyMuPDF module
from PIL import Image
import sys, json

# Improve 1: Calculate the distance between two lines (font height) to get the appropriate offset 
# Improve 2: Search start word for each question in the Exam by Regex (or some format)

# Improve 3: allow user input to set format/Regex/Offset

# Improve resolution
# List of accepted start words

# Read text from image in pdf


def hightlightPdf(pdfName,word):
    # read file name from user input
    # ifile = sys.argv[1]
    # ofile = ifile + ".pdf"

    c=0
    # opening the pdf file using fitz
    fitz_doc=fitz.open(pdfName)

    # getting first page of the doc
    fitz_page=fitz_doc[0]
    # finding all instances where the searchword is found
    text_instances=fitz_page.searchFor(word)

    # Iterating through each text instances  
    for text_cord in text_instances:
        c=c+1
        pdfPath = "./" + pdfName + ".pdf"
        # To add highlight(Rectangle Annotation) around the search word
        highlight = fitz_page.addRectAnnot(text_cord)
        # getting the bounding box cordinate
        x0,y0,x1,y1=highlight.rect
        # here i set the cropping area around the annotated text
        fitz_page.setCropBox(fitz.Rect(x0+600,y0+600,x0-600,y0-600))
        #
        pix=fitz_page.getPixmap()
        print(fitz_page.number)
        base_name_highlight="output"+str(c)+".png"
        # saving the cropped area as png file
        pix.writeImage("./crop_images/"+base_name_highlight)
        # Deleting the marked annotation which helps me to avoid duplicate annotation inside a cropped area,
        # when starting to annotate the next occurence of the word to annotate while iterating.
        fitz_page.deleteAnnot(highlight)


def cropFixCoordImage(pdfName, word):
    doc = fitz.open(pdfName)    # Mở file pdf

    i = 0
    for page in doc:   
        wordlist = page.getTextWords()  # Lấy toàn bộ các word kèm vị trí theo khung chữ nhật  (khung chữ nhật chứa word)
        # print(wordlist[0])

        # hoặc, lấy toàn bộ các word theo dòng kèm vị trí của dòng theo khung chữ nhật (kèm cả hình chữ nhật)
        blocklist = page.getTextBlocks()
        # print(blocklist)

        #crop ra ảnh từ một position nào đó (ví dụ lấy câu số 10)
        page.setCropBox(fitz.Rect(42.599998474121094, 133.0767822265625, 467.3800048828125, 145.2701416015625))

        pix=page.getPixmap()

        output = "outfile" + str(i) + ".png"
        pix.writePNG(output)

        i += 1


def cropImage(pdfName, word = "Câu"):
    doc = fitz.open(pdfName)    # Mở file pdf

    a = [doc[0]]
    i = 0
    for page in a:   
        # text_instances=page.searchFor(word)
        # y0 = text_instances[1][1]
        # y1 = text_instances[1][3]
        # offset = (y1 - y0)/2
        # print(offset)
        # # print(text_instances[0:3])

        wordlist = page.getTextWords()  # Lấy toàn bộ các word kèm vị trí theo khung chữ nhật  (khung chữ nhật chứa word)
        # print(json.dumps(wordlist[0:3], sort_keys=True, indent=4))

        # hoặc, lấy toàn bộ các word theo dòng kèm vị trí của dòng theo khung chữ nhật (kèm cả hình chữ nhật)
        blocklist = page.getTextBlocks()
        print(json.dumps(blocklist[5:8], sort_keys=True, indent=4))

        index = [i for i, x in enumerate(blocklist) if "Câu" in x[4]][2]

        y0 = blocklist[index - 1][3]
        y1 = blocklist[index][1]
        offset = (y1 - y0)/2

        print(offset)
        print(json.dumps(blocklist[index], sort_keys=True, indent=4))


        #crop ra ảnh từ một position nào đó (ví dụ lấy câu số 10)
        page.setCropBox(fitz.Rect(42.599998474121094, 133.0767822265625, 467.3800048828125, 145.2701416015625))
        pix=page.getPixmap()

        output = "outfile" + str(i) + ".png"
        pix.writePNG(output)

        i += 1


def cropPdf1(
    pdfName,
    directory,
    offsetUp = 5,
    offsetDown = 5,
    startWords = ["Cau"],
    notStartWords = ["Trang"]):

    i = 0
    # opening the pdf file using fitz
    fitz_doc=fitz.open(pdfName)    
    images = []
    widths = []
    heights = []

    # Iterating through each text instances  
    for fitz_page in fitz_doc:
        # finding all instances where the searchword is found
        text_instances=fitz_page.search_for(startWords[0])
        textCorY2 = None
        c=0

        blocklist = fitz_page.getTextBlocks()
        endPageY = blocklist[len(blocklist) - 1][3]
        startPageY = blocklist[0][1]            
        indices = [t for t, x in enumerate(blocklist) if word in x[4]]

        print(len(indices), len(text_instances))

        for text_cord in text_instances:
            c=c+1
            # index = indices[c - 1]

            if i > 0 and c == 1 and startPageY + offset < text_cord[1] - offset:
                fitz_page.setCropBox(fitz.Rect(text_cord[0], startPageY - offset, text_cord[2] + 1000, text_cord[1] - offset))
                
                # get pix map
                pix=fitz_page.getPixmap()
                base_name_highlight="output_"+str(i)+".png"
                
                tempImg = Image.frombytes("RGB", [pix.width, pix.height], pix.samples)
                images.append(tempImg)
                widths.append(pix.width)
                heights.append(pix.height)

                maxWidth = max(widths)
                totalHeight = sum(heights)
                new_im = Image.new('RGB', (maxWidth, totalHeight))
                x_offset = 0
                for im in images:
                    new_im.paste(im, (0,x_offset))
                    x_offset += im.size[1]

                new_im.save("./crop_images/"+base_name_highlight)

            if c == len(text_instances):
                textCorY2 = endPageY + offset * 1.6
            else:
                textCorY2 = text_instances[c][1]

            # here i set the cropping area around the annotated text
            fitz_page.setCropBox(fitz.Rect(text_cord[0],text_cord[1] - offset,text_cord[2] + 1000,textCorY2 - offset))

            # get pix map
            pix=fitz_page.getPixmap()
            if c == len(text_instances):
                preTempImg = Image.frombytes("RGB", [pix.width, pix.height], pix.samples)
                images = [preTempImg]
                widths = [pix.width]
                heights = [pix.height]

            i += 1
            base_name_highlight="output_"+str(i)+".png"
            # saving the cropped area as png file
            pix.writeImage("./crop_images/"+base_name_highlight)
       

def getMinCoorX(blockList):
    mapBlockList = [x[0] for t, x in enumerate(blockList)]
    return min(mapBlockList)

def getMaxCoorX(blockList):
    mapBlockList = [x[2] for t, x in enumerate(blockList)]
    return max(mapBlockList)

def getMinCoorY(blockList, minY = 0):
    mapBlockList = [x[1] for t, x in enumerate(blockList) if x[1] > minY]
    return min(mapBlockList)

def getMaxCoorY(blockList):
    mapBlockList = [x[3] for t, x in enumerate(blockList)]
    return max(mapBlockList)

def getCoorX(blocklist, notStartWords):
    blocklist = [x for t, x in enumerate(blocklist) if (not x[4].strip().startswith(tuple(notStartWords)) and x[4].strip())]
    
    textCorX1 = getMinCoorX(blocklist)
    textCorX2 = getMaxCoorX(blocklist)

    return [textCorX1, textCorX2]

def getDocCoorX(doc, notStartWords):
    coorX1List = []
    coorX2List = []
    for page in doc:
        blocks = page.get_text_blocks()
        blocklist = [x for t, x in enumerate(blocks) if (not x[4].strip().startswith(tuple(notStartWords)) and x[4].strip())]
        
        coorX1List.append(getMinCoorX(blocklist))
        coorX2List.append(getMaxCoorX(blocklist))

    return [min(coorX1List), max(coorX2List)]


def cropPdf2(
    offsets,
    pdfName, 
    startWords, 
    imgPrefix,
    directory,
    notStartWords):
    # opening the pdf file using fitz
    fitz_doc=fitz.open(pdfName)    

    # test area
    fitz_page0=fitz_doc[0]
    text_instances1=fitz_page0.getTextWords()
    text_instances2=[x for t, x in enumerate(text_instances1) if x[4].strip().startswith(tuple(startWords))]

    # print(json.dumps(text_instances1[50:100], sort_keys=True, indent=4))

    # get min and max coordinate for x-axis
    zoom = 2    # zoom factor
    mat = fitz.Matrix(zoom, zoom)

    images = []
    widths = []
    heights = []
    
    # Iterating through each page
    i = 0
    test = 0
    for fitz_page in fitz_doc:
        test += 1
        blocklist = fitz_page.getTextBlocks()

        blocklist = [x for t, x in enumerate(blocklist) if ((not x[4].strip().startswith(tuple(notStartWords))) and x[4].strip())]

        if (len(blocklist) == 0): continue
        textCorX1, textCorX2 = getCoorX(blocklist, notStartWords)

        indices = [t for t, x in enumerate(blocklist) if x[4].strip().startswith(tuple(startWords))]
        
        # if (test == 1):
        #     print(json.dumps(blocklist[9:15], sort_keys=True, indent=4))
        
        textCorY1 = getMinCoorY(blocklist)
        textCorY2 = getMaxCoorY(blocklist)

        for j in range(0, len(indices) + 1):
            text_cord = []
            if (j < len(indices)):
                text_cord = blocklist[indices[j]]
            elif (len(indices) != 0): break

            if i > 0 and j == 0 and (len(indices) == 0 or textCorY1 < text_cord[1]):
                if len(indices) > 0:
                    textCorY2 = blocklist[indices[0]][1]

                # crop the area
                fitz_page.setCropBox(fitz.Rect(textCorX1, textCorY1, textCorX2, textCorY2))
                # get pix map
                pix=fitz_page.getPixmap(matrix = mat)

                tempImg = Image.frombytes("RGB", [pix.width, pix.height], pix.samples)
                images.append(tempImg)
                widths.append(pix.width)
                heights.append(pix.height)

                maxWidth = max(widths)
                totalHeight = sum(heights)
                new_im = Image.new('RGB', (maxWidth, totalHeight))
                x_offset = 0
                for im in images:
                    new_im.paste(im, (0,x_offset))
                    x_offset += im.size[1]

                base_name_highlight="output2_"+str(i)+".png"
                new_im.save(f'./{directory}/' + base_name_highlight)

            if j == len(indices) - 1:
                textCorYPre = 0
                if j > 0:
                    textCorYPre = blocklist[indices[j - 1]][1]

                textCorY1 = getMinCoorY(blocklist[indices[j] :], minY = textCorYPre)
                textCorY2 = getMaxCoorY(blocklist)
            elif j < len(indices) - 1:
                textCorY1 = getMinCoorY(blocklist[indices[j] : indices[j + 1]])
                textCorY2 = getMaxCoorY(blocklist[indices[j] : indices[j + 1]])
            else: break

            # here i set the cropping area around the annotated text
            fitz_page.setCropBox(fitz.Rect(textCorX1, textCorY1, textCorX2, textCorY2))
            # get pix map
            pix=fitz_page.getPixmap(matrix = mat)

            if j == len(indices) - 1:
                preTempImg = Image.frombytes("RGB", [pix.width, pix.height], pix.samples)
                images = [preTempImg]
                widths = [pix.width]
                heights = [pix.height]

            i += 1
            # saving the cropped area as png file
            base_name_highlight="output2_"+str(i)+".png"
            pix.writeImage(f'./{directory}/' + base_name_highlight)


def SortBlocks(blocks):
    '''
    Sort the blocks of a TextPage in ascending vertical pixel order,
    then in ascending horizontal pixel order.
    This should sequence the text in a more readable form, at least by
    convention of the Western hemisphere: from top-left to bottom-right.
    If you need something else, change the sortkey variable accordingly ...
    '''

    sblocks = []
    for b in blocks:
        x0 = str(int(b["bbox"][0]+0.99999)).rjust(4,"0") # x coord in pixels
        y0 = str(int(b["bbox"][1]+0.99999)).rjust(4,"0") # y coord in pixels
        
        sortkey = y0 + x0                                # = "yx"
        sblocks.append([sortkey, b])

    sblocks.sort(key=lambda x: (x[0], x[1]['bbox'][1], x[1]['bbox'][0]), reverse=False)
  
    return [b[1] for b in sblocks] # return sorted list of blocks

def SortLines(lines):
    ''' Sort the lines of a block in ascending vertical direction. See comment
    in SortBlocks function.
    '''

    slines = []
    for l in lines:
        y0 = l["bbox"][1]
        slines.append([y0, l])

    slines.sort(key=lambda x: (x[0], x[1]['bbox'][0]), reverse=False)

    return [l[1] for l in slines]

def SortSpans(spans):
    ''' Sort the spans of a line in ascending horizontal direction. See comment
    in SortBlocks function.
    '''
    sspans = []
    for s in spans:
        x0 = s["bbox"][0]
        sspans.append([x0, s])
    sspans.sort()
    return [s[1] for s in sspans]

def FilterTextBlocks(textBlocks, startWords):
    newBlocks = []

    for b in textBlocks:
        if "lines" in b:
            for l in b["lines"]:
                if "spans" in l:
                    for s in l["spans"]:
                        if "text" in s and s["text"].strip().startswith(tuple(startWords)):
                            newBlocks.append(b)

    return newBlocks


def cropPdf3(
    minLineHeight,
    offsets,
    pdfName, 
    startWords, 
    imgPrefix,
    directory,
    notStartWords):

    doc = fitz.open(pdfName)
    pages = doc.page_count

    zoom = 2    # zoom factor
    mat = fitz.Matrix(zoom, zoom)

    images = []
    widths = []
    heights = []
    image_numb = 0
    textCorX1, textCorX2 = getDocCoorX(doc, notStartWords)

    for p_numb in range(pages):
        fitz_page = doc[p_numb]

        # get min and max coordinate for x-axis
        blocklist = fitz_page.get_text_blocks()
        blocklist = [x for t, x in enumerate(blocklist) if (not x[4].strip().startswith(tuple(notStartWords)) and x[4].strip())]

        textCorY1 = getMinCoorY(blocklist)
        textCorY2 = None

        pg = doc.load_page(p_numb)                         # load page number i
        text = pg.get_text('json')           # get its text in JSON format

        pgdict = json.loads(text)                   # create a dict out of it

        # if (p_numb == 0):
        #     print(json.dumps(pgdict["blocks"], sort_keys=True, indent=4))
        textBlocks = FilterTextBlocks(pgdict["blocks"], startWords)
        blocks = SortBlocks(textBlocks)        # now re-arrange ... blocks

        # print(blocks)

        textCorY = []
        for b in blocks:
            lines = []

            if "lines" in b:
                lines = SortLines(b["lines"])            # ... lines
            else: continue

            for l in lines:
                spans = SortSpans(l["spans"])        # ... spans
                s = spans[0]
                spanCoors = [x["bbox"] for t, x in enumerate(spans)]

                # if p_numb == 1:
                #     print(spans)
                #     print("\n")

                if s["text"].strip().startswith(tuple(startWords)):
                    textCorY.append(getMinCoorY(spanCoors))

        for y_numb in range(len(textCorY)):
            if p_numb > 0 and y_numb == 0 and textCorY[0] - textCorY1 >= minLineHeight :
                textCorY2 = textCorY[0]
                if str(image_numb) in offsets:
                    textCorY2 = textCorY2 - offsets.get(str(image_numb))

                # crop the area
                fitz_page.setCropBox(fitz.Rect(textCorX1, textCorY1, textCorX2, textCorY2))
                # get pix map
                pix=fitz_page.getPixmap(matrix = mat)

                tempImg = Image.frombytes("RGB", [pix.width, pix.height], pix.samples)
                images.append(tempImg)
                widths.append(pix.width)
                heights.append(pix.height)

                maxWidth = max(widths)
                totalHeight = sum(heights)
                new_im = Image.new('RGB', (maxWidth, totalHeight))
                x_offset = 0
                for im in images:
                    new_im.paste(im, (0,x_offset))
                    x_offset += im.size[1]

                base_name_highlight= imgPrefix + str(image_numb - 1) + ".png"
                new_im.save(f'./{directory}/' + base_name_highlight)

            textCorY1 = textCorY[y_numb]

            if (y_numb < len(textCorY) - 1):
                textCorY2 = textCorY[y_numb + 1]
            else:
                textCorY2 = getMaxCoorY(blocklist)
            
            if str(image_numb + 1) in offsets and y_numb != len(textCorY) - 1:
                textCorY2 = textCorY2 - offsets.get(str(image_numb + 1))
            if str(image_numb) in offsets:
                textCorY1 = textCorY1 - offsets.get(str(image_numb))

            # here i set the cropping area around the annotated text
            fitz_page.setCropBox(fitz.Rect(textCorX1, textCorY1, textCorX2, textCorY2))
            # get pix map
            pix=fitz_page.getPixmap(matrix = mat)

            # saving the cropped area as png file
            base_name_highlight = imgPrefix + str(image_numb) + ".png"
            pix.writeImage(f'./{directory}/' + base_name_highlight)

            if y_numb == len(textCorY) - 1:
                preTempImg = Image.frombytes("RGB", [pix.width, pix.height], pix.samples)
                images = [preTempImg]
                widths = [pix.width]
                heights = [pix.height]
        
            image_numb += 1

                   


# fileName = "pdf_docs/Mau-PDF/kiem-tra-giai-tich-12-chuong-1-nam-2019-2020-truong-nguyen-trung-thien-ha-tinh.pdf"
fileName = "pdf_docs/3 [Tailieuvui.com]_de-thi-thpt-quoc-gia-2020-mon-toan-cua-bo-gddt-de-104.pdf"


fileName1 = "pdf_docs/1_De-Chinhthuc-Vatli-K18-M224-pdf.pdf"
fileName2 = "pdf_docs/1-De-thi-tham-khao-TN-THPT-2020-Ngu-van.pdf"
fileName3 = "pdf_docs/2 Đề mẫu chuẩn (convert PDF).pdf"
fileName4 = "pdf_docs/3 [Tailieuvui.com]_de-thi-thpt-quoc-gia-2020-mon-toan-cua-bo-gddt-de-104.pdf"
fileName5 = "pdf_docs/4 bo-de-thi-thu-thpt-quoc-gia-nam-2017-mon-toan-so-1.pdf"
fileName6 = "pdf_docs/5-De-thi-tham-khao-TN-THPT-2020-Sinh-hoc.pdf"
fileName7 = "pdf_docs/Mau-PDF/20-sample-ket-01.pdf"
fileName8 = "pdf_docs/Mau-PDF/dap-an-de-tham-khao-2021-mon-tieng-anh.pdf"
fileName9 = "pdf_docs/Mau-PDF/de-hsg-toan-10-nam-2020-2021-cum-thpt-huyen-yen-dung-bac-giang.pdf"


directory = "images_output/Mau-PDF"


directory1 = "images_output/1_De-Chinhthuc-Vatli-K18-M224-pdf"
directory2 = "images_output/1-De-thi-tham-khao-TN-THPT-2020-Ngu-van"
directory3 = "images_output/2 Đề mẫu chuẩn (convert PDF)"
directory4 = "images_output/3 [Tailieuvui.com]_de-thi-thpt-quoc-gia-2020-mon-toan-cua-bo-gddt-de-104"
directory5 = "images_output/4 bo-de-thi-thu-thpt-quoc-gia-nam-2017-mon-toan-so-1"
directory6 = "images_output/5-De-thi-tham-khao-TN-THPT-2020-Sinh-hoc"
directory7 = "images_output/20-sample-ket-01"
directory8 = "images_output/dap-an-de-tham-khao-2021-mon-tieng-anh"
directory9 = "images_output/de-hsg-toan-10-nam-2020-2021-cum-thpt-huyen-yen-dung-bac-giang"




cropPdf3(
    minLineHeight = 10,
    offsets = { "1": 2, "2": 10, "3": 2, "4": 10, "5": 3, 
    "6": 3, "7": 2, "8": 1, "9": 5, "16": 5, "26": 6, "27": 10 },
    startWords = ["Cau", "Câu", "Question", "Questions", "Bài", "WRITING TASK", "PART", "ĐỌC HIỂU"],
    notStartWords = ["Trang", "VnDoc"], 
    pdfName = fileName, 
    imgPrefix = "output_",
    directory = directory)



# cropPdf3(
#     minLineHeight = 10,
#     offsets = { "2": 1, "3": 6, "4": 10, "6": 3, "7": 2, "8": 5, "9": 5 },
#     startWords = ["Cau", "Câu", "Question", "Questions", "Bài", "WRITING TASK", "PART", "ĐỌC HIỂU"],
#     notStartWords = ["Trang", "VnDoc"], 
#     pdfName = fileName, 
#     imgPrefix = "output_",
#     directory = directory)


# cropImage(fileName3)
